import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import App from "./App";
import About from "./Nav/About";
import NotFound from "./Nav/notFound";
import Profile from "./Nav/Profile";
import * as serviceWorker from "./serviceWorker";
import 'bootstrap/dist/css/bootstrap.min.css';

const routing = (
    <Router>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link className="navbar-brand" to="/">
                Nav
    </Link>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link className="navbar-brand" to="/">
                            Home
            </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/profile">
                            Profile
            </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/about">
                            About
            </Link>
                    </li>
                </ul>
            </div>
        </nav>
        <Switch>
        <Route exact path="/" component={App} />
        <Route path="/about/:param" component={About} />
        <Route path="/profile/" component={Profile} />
        <Route component={NotFound} />
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));
// ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
